import Foundation


//print("for 迴圈")
for i in 0..<3{
    print("#1 index: \(i)")
}
for i in 0...3{
    print("#2 index: \(i)")
}

let lstScore = [0,1,2,3,4,5]
var teamScore = 0
for score in lstScore {
    print("#Score : \(score)")
}

let lstNumArrs = [
    "a" : [1,2,3],
    "b" : [1,3,5],
    "c" : [1,5,7]
]

var largest = 0
for(index,number) in lstNumArrs{
    print("index: \(index)")
    for num in number{
        print("number: \(num)")
    }
}

//print("if-else 敘述")

var count = 5
if(count > 3){
    print("Bigger than 3")
}else{
    print("Small than 3")
}

var optionName: String? = "OwO / " // nil
//  var optionName: String? = nil // nil
var greeting = " Hi!"
if let name = optionName {
    greeting = "Hello, \(name)"
}else{
    greeting = "Ops"
}
print(greeting)

//print("switch 敘述")

var tag = "OwO"

switch tag {
case "OwO":
    print("OwO/")
default:
    print("Default")
}

var num = 12

switch num {
case 5 :
    print(" is five")
case 6...10:
    print(" is six to ten")
case 11..<15:
    print(" is ten or big tan 15.")
default:
    print("Default")
}

let ctrl = "OwO RwR"
switch ctrl{
case "OwO":
    print("OwO !")
case "Hi","Ha":
    print("OwO / Hi")
case let x where x.hasSuffix("RwR"):
    print("OwO /RRR \(x)")
default:
    print("OwO / Default")
}

// print("nil")

var optionalString: String? = "Hello" // nil
print(optionalString == nil)

let nickName : String? = nil
//  let nickName : String? = "Message"
let fullName = "John Appleseed"
let informalGreeting = "Hi \(nickName ?? fullName)"


// print(while)
var n = 2
while n<100{
    n *= 2
}
print(n)

// print(repeat)
var m = 2
repeat {
    m *= 2
}while m<100
print(m)

