//: ## Function
import Foundation
//: 簡單的方法
func printMsg(){
    print("OwO");
}

printMsg()
//: 帶參數的方法，回傳值
func printMsg(msg:String) {
    print(msg)
}

printMsg(msg:"Hello")

//: 帶雙參數的方法
func printMsg(level : String , msg :  String){
    print("logger.\(level) : \(msg)")
}

printMsg(level: "info",msg : "Hello , OwO / " )

//: 不太清楚 on & by 作用
func printMsg1(custom level : String,_ msg :  String){
    print("logger.\(level) : \(msg)")
}

printMsg1(custom : "warn", "OwO / 1")

func printMsg2(by level : String = "OwO" ,_ msg :  String){
    print("logger.\(level) : \(msg)")
}

printMsg2(by : "debug", "OwO / 2")
printMsg2("OwO / 3")

//: 參數回傳
func addTwoNumber(num1 : Int , num2 : Int) -> Int{
    return num1 + num2
}

var res = addTwoNumber(num1:1,num2:1)
print("res -> \(res)")

//: 陣列參數，回傳多參數
func addAllNumber(lstNum : [Int]) ->
    (sum: Int , max: Int , min: Int) {
    var min = lstNum[0]
    var max = lstNum[0]
    var sum = 0
    for num in lstNum {
        if num > max {
            max = num
        }
        
        if num < min {
            min = num
        }
        sum += num
    }
    
    return (sum , max , min)
}

var result = addAllNumber(lstNum: [1,2,3,4,5,6,7,8,9,10])
print("result: \(result)")
print("Sum: \(result.sum) : \(result.0)")
print("Max: \(result.max) : \(result.1)")
print("Min: \(result.min) : \(result.2)")

//: 參數長度不限定
func sum(nums: Double...) -> Double {
    var sum: Double = 0.0
    for num in nums {
        sum += num
    }
    return sum
}

var calSum = sum(nums: 1.0, 2.0, 3.0, 4.0)
print("sum -> \(calSum)")

//: Call By Reference
func addCount(num:inout Int) {
    num += 1
}

var num = 1
addCount(num: &num)
print("num -> \(num)")

