import Foundation

var map = [
    "a" : "AAA" ,
    "b" : "BBB" ,
    "c" : "CCC" ,
    "d" : "DDD" ,
    "e" : "EEE"
]

print("#1:")
print(map)
//print(map["a"]) // warning

map["a"] = "OwO"
print("#2:")
print(map)
map.updateValue("RwR", forKey: "a")
print("#3:")
print(map)

if let a = map["a"] {
    print(a)
}
map.removeValue(forKey: "e")
print("#4:")
print(map)

map.updateValue("X", forKey: "x")
map["r"] = "R"
print("#5:")
print(map)

var obj:[String:String] = [
    "a" : "AAA" ,
    "b" : "BBB" ,
    "c" : "CCC" ,
]
print("obj")
print(obj)

print("#1:")
for(key,value) in obj{
    print("key : \(key) value: \(value)")
}
print("#2:")
for key in obj.keys{
    print("key : \(key)")
}
print("#3:")
for value in obj.values{
    print("value : \(value)")
}

let keys = Array(obj.keys)
print("keys")
print(keys)
let vals = Array(obj.values)
print("vals")
print(vals)

obj = [:]
print("clear")
print(obj.count)
print(obj)
