import Foundation

var strArr = ["a","b","c"]
var strArr2:[String] = ["a","b","c"]
var strArr3 = [String]()
var strArr4 = [String](repeating: "OwO", count: 3)

strArr += ["d"]
strArr.append("e")
strArr += ["f","g"]

strArr3 = strArr + strArr2

print(strArr[0])
print(strArr.first!)
print(strArr[strArr.endIndex-1])
print(strArr.last!)

strArr[0] = "r"

strArr[0..<1] = ["1"]
strArr[0...1] = ["1","2"]
strArr[0...3]
strArr[0..<strArr.endIndex-1]
if(strArr.isEmpty){
    print("Empty")
}else{
    print("Has Value")
}

strArr.insert("x", at: 0)
strArr.remove(at: 0)
print(strArr)
