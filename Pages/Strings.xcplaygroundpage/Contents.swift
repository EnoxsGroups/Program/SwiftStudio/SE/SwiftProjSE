import Foundation
var str = "Hello, OwO"
var icon = "😀"
var msg = str + " " +  icon
print(msg)

var len = str.count
print("len: \(len)" )

msg = str.lowercased()
print(msg)

msg = str.uppercased()
print(msg)

msg = "OwO"
for c in msg{
    print(c)
}

if(msg == "OwO"){
    print("OwO / ")
}

if(msg.hasPrefix("Ow")){
    print("OwO / #1")
}

if(msg.hasPrefix("wO")){
    print("OwO / #2")
}

let count = 0
print("Count : \(count) .")


let dollarSign = "\u{24}"        // $,  Unicode 純量 U+0024
print(dollarSign)
let blackHeart = "\u{2665}"
print(blackHeart)


