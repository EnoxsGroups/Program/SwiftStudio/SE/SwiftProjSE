class VersionInfo {
    var v1Count:Int =  1
    var v2Count:Int = 0
    var v3Count:Int = 1
    var version: (Int , Int ,Int){
        get{
            return (v1Count,v2Count,v3Count)
        }
        set(val){
            self.v1Count = val.0
            self.v2Count = val.1
            self.v3Count = val.2
        }
    }
    func printMsg() -> String{
        return "\(v1Count).\(v2Count).\(v3Count)" 
    }
}
class AppInfo {
    lazy var versionInfo = VersionInfo()
    init(_ appName: String ,_ appVersion:String){
        self.appName = appName
        self.appVersion = appVersion
    }
    var appName:String?
    var appVersion:String?
    var appRemark:[String]?
}
var appInfo = AppInfo("OwO","1.0.1")
print(appInfo.appName)
print(appInfo.appVersion)
print(appInfo.versionInfo.v1Count)

print(appInfo.versionInfo.printMsg())
appInfo.versionInfo.version = (2,1,5)
print(appInfo.versionInfo.printMsg())
