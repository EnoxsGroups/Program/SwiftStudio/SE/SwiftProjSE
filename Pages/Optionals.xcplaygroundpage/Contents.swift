//:Optionals ， 選配器

var one = "1"
var intVal = Int(one)

if let intVal = Int(one){
    print(intVal)
}

var msg = "HelloOwO"
var val = Int(msg)

if val == nil {
    print("could not convert the string to an int")
}

var specMsg:String? = nil
print(specMsg)
print(specMsg == nil)

specMsg = "Hello"
print(specMsg)
